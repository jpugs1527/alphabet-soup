# Alphabet-Soup

Alphabet-Soup is a tool that allows users to generate an answer key to a given word search.

Users can feed in properties of a word search via a .txt file.  Properties must include the size of the matrix as the first line in the form #x#, the matrix that represents the word search grid, followed by the words to search for.  Given these properties, a user can generate an answer key that provides the index of the first and last character of the word if it is found.

## Prerequisites
Before you begin, ensure you have met the following requirements:
* You have installed Python 3.9.2 and pip 21.0.1
* You have a Windows/Linux/Mac machine.

## Using Alphabet-Soup
To use Alphabet-Soup, follow these steps:
* Clone the repo
* Go into the directory with `wordsearch.py` 
* Run `wordsearch.py` with the text file you wish to get an answer key for:
```
git clone <repository_name>
cd enlighten-programming-challenge
python3 wordsearch.py -f <your_word_search_file.txt>
```

## Contributing to Alphabet-Soup
To contribute to Alphabet-Soup, follow these steps:
1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

## Developer Reference
It is recommended to use a virtual environment while developing on this project.  To create and activate the virtual environment, run:  
```
python3 -m venv /path/to/new/virtual/environment
source <venv>/bin/activate
```  
### Installing dependencies from requirements.txt
* After setting up your virtual environment, external dependencies can be installed using the included `requirements.txt` file.
* To install, run:  
```
pip3 install -r requirements.txt
```
### Running wordsearch.py
```
python3 wordsearch.py -f <your_word_search_file.txt>
```
### Testing wordsearch.py
```
python3 test_word_search.py
```
## Design Decisions
#### WordSearch Class
* I chose to take an OOP approach to deal with the text file to ensure that this object could potentially be reused elsewhere to extend functionality. This helped to simplify the problem and provide the necessary parameters to be able to search for the words.
#### Brute Force Search
* For this challenge, I chose to go with a brute force search as that was sufficient for the task at hand.  Brute force search of a 2D array is a common operation most developers should be able to understand.  Using a library would add complexity for a developer trying to maintain/debug if they are not familiar with that library.

## Assumptions
* Valid file format

## Dependencies
* sys - Used for validating that required arguments are passed when running the script. This comes installed with Python 3.9.2.
* itertools - Used to get the cartesian product of the matrix for traversing the matrix. (Neater code, fewer nested loops.) This comes installed with Python 3.9.2.
* argparse - Used for parsing out the arguments passed on run to handle the file as input.  This comes installed with Python 3.9.2.
* unittest - The library that is used for writing unit tests to test the functions and methods of the answer key generator. This comes installed with Python 3.9.2.
* io -  Used along side unittest to capture printed output.  This output is compared with expected output to ensure correctness of the program through a test. This comes installed with Python 3.9.2.
* pdoc - Used to generate API documentation for the script. This must be installed separately:  
```
pip install pdoc
```

## Potential Improvements
* One area of improvement would be to use Numpy for array traversals.  Numpy allows for vector operations which are much quicker than list operations.  Search using Numpy has the potential to be faster than brute force as I would pull all possible combinations of rows, columns, and diagonals to then search for a substring rather than checking character by character.  Using Numpy, search would be less likely to slow down when handling matricies of large size.
* Testing could improve a good bit.  There are some tests that are really only success tests and do not thoroughly test the script.
* Currently, the program returns the first location of the word as soon as it is found. It could potentially be improved to find all occurrences of the word.


