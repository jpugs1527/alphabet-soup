#!/usr/bin/env python3

import unittest
import sys
import io

import wordsearch

class TestAnswerKeyGenerator(unittest.TestCase):

    def test_row_count(self):
        word_search = wordsearch.WordSearch("./test_files/word_search.txt")
        self.assertEqual(word_search.rows, 5)

    def test_column_count(self):
        word_search = wordsearch.WordSearch("./test_files/word_search.txt")
        self.assertEqual(word_search.cols, 5)

    def test_get_word_search_matrix(self):
        word_search = wordsearch.WordSearch("./test_files/word_search.txt")
        matrix = word_search.matrix
        expected_matrix = [["H", "A", "S", "D", "F"], ["G", "E", "Y", "B", "H"], ["J", "K", "L", "Z", "X"], ["C", "V", "B", "L", "N"], ["G", "O", "O", "D", "O"]]
        self.assertListEqual(matrix, expected_matrix)
    
    def test_get_words_to_search_for(self):
        word_search = wordsearch.WordSearch("./test_files/word_search.txt")
        word_list = word_search.words
        expected_word_list = ["HELLO", "GOOD", "BYE"]
        self.assertListEqual(word_list, expected_word_list)
    
    def test_brute_force_search(self):
        word_search = wordsearch.WordSearch("./test_files/word_search.txt")
        word = "HELLO"
        word_coords = word_search.brute_force_search(word)
        expected_coords = [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4]]
        
        self.assertEqual(word_coords, expected_coords)

    def test_file_not_found_exception(self):
        word_search = wordsearch.WordSearch("./test_files/nonexistent.txt")
        self.assertRaises(FileNotFoundError)

if __name__ == "__main__":
    unittest.main()