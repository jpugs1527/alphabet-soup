#!/usr/bin/env python3

import sys
import itertools
import argparse

class WordSearch:
    """ 
    A class used to represent the contents of the word search file.

    ...

    Attributes
    ----------
    text_file: str
        A file path of type string to reference the word search text file.

    Methods
    -------
    initialize() - This method is called on object creation to set the value
    of all members of the object based on the file passed in.
    brute_force_search() - This method takes in a word as a parameter and iteratively 
    searches the matrix to try to find the starting and ending indicies of the
    first and last letter of the word.
    """

    def __init__(self, text_file):
        """
        Parameters
        ----------
        text_file: str
            A file path of type string to reference the word search text file.
        """
        self.rows = 0
        self.cols = 0
        self.matrix = list()
        self.words = list()
        self.initialize(text_file)

    def initialize(self, text_file):
        """
        Parameters
        ----------
        text_file: str
            A file path of type string to reference the word search text file.

        Sets all attributes of the WordSearch object.
        """
        try:
            with open(text_file, "r") as f:
                # Get the matrix size from the first line of the file
                size = f.readline().strip().split("x")
                self.rows = int(size[0])
                self.cols = int(size[1])

                # Create the word search matrix to be searched
                for line in range(self.rows):
                    current_line = f.readline()
                    letters = current_line.split()
                    self.matrix.append(letters)

                # Get all of the words to search for
                word = True
                while word:
                    word = f.readline()
                    if word:
                        self.words.append(word.strip().replace(" ", ""))

        except FileNotFoundError:
            print(f"File not found {text_file}")
    

    def brute_force_search(self, word):
        """
        Creates a WordSearch object and searches through the word search matrix
        to try to find the word.  If it finds a word, it returns a list of the
        coordinates that correspend to the location of the letters in the word.

        Parameters
        ----------
        word - A string to search the word search matrix for.
        """

        # Tuple used to set offset for searching around a specific index
        list_of_offsets_for_search = [(1,1), (1,-1), (1,0), (-1,1), (-1,-1), (-1,0), (0,1), (0,-1)]

        word_coords = []
        for row, col in itertools.product(range(self.rows), range(self.cols)):
            
            # Traverse the matrix in a particular direction from the current index
            for offset in list_of_offsets_for_search:

                # Initialize current row and col based on iteration of traversal
                curr_row = row
                curr_col = col

                # Traverse the word character by character to search for a match
                for i in range(len(word)):
                    # If valid current index, check if the current letter matches the value being checked 
                    # from the matrix and append to list of coordinates
                    if 0 <= curr_row < self.rows and 0 <= curr_col < self.cols:
                        if word[i] == self.matrix[curr_row][curr_col]:
                            # Stores location of letters in the list
                            word_coords.append([curr_row,curr_col])
                            
                            # Continues movement in direction of matching letters
                            curr_row += offset[0]
                            curr_col += offset[1]
                        else:
                            # If a letter does not match the next in the word, clear the list
                            word_coords = []
                            break
                    else:
                        # If out of bounds, clear the list for next offset
                        word_coords = []
                        break  
                    
                if len(word) == len(word_coords):
                    return word_coords
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", 
                        dest="text_file",
                        help=
                        """
                        name of text file to parse
                        """,
                        type=str)
    args = parser.parse_args()
    if len(sys.argv) == 1:
        parser.print_help()
    else:
        word_search = WordSearch(args.text_file)
        for word in word_search.words:
            word_coords = word_search.brute_force_search(word)
            if word_coords:
                print(f"{word} {str(word_coords[0][0])}:{str(word_coords[0][1])} {str(word_coords[-1][0])}:{str(word_coords[-1][1])}")
